'use strict';
import Joi from '@hapi/joi';
import { min } from 'lodash';
import { JoiObjectId, JoiUrlEndpoint } from '../../helpers/validator';

export default {
  latitude: Joi.object().keys({
    tag: Joi.number().precision(4),
  }),
  longitude: Joi.object().keys({
    tag: Joi.number().precision(4),
  }),
  address: Joi.object().keys({
    tag: Joi.string().min(5).max(45),
  }),
  firstName: Joi.object().keys({
    tag: Joi.string().min(3),
  }),
  lastName: Joi.object().keys({
    tag: Joi.string().min(3).max(20),
  }),
  email: Joi.object().keys({
    tag: Joi.string().email(),
  }),
  pagination: Joi.object().keys({
    pageNumber: Joi.number().required().integer().min(1),
    pageItemCount: Joi.number().required().integer().min(1),
  }),
};
