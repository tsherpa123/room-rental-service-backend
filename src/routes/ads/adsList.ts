'use strict';
import express from 'express';
import { SuccessResponse } from '../../core/ApiResponse';
import { NoDataError, BadRequestError } from '../../core/ApiError';
import BlogRepo from '../../database/repository/AdsRepo';
import { Types } from 'mongoose';
import validator, { ValidationSource } from '../../helpers/validator';
import schema from './schema';
import asyncHandler from '../../helpers/asyncHandler';
import AdsRepo from '../../database/repository/AdsRepo';

const router = express.Router();

router.get(
  '/listAll',
  validator(schema.pagination, ValidationSource.QUERY),
  asyncHandler(async (req, res) => {
    const ads = await AdsRepo.findByPagination(
      parseInt(req.query.pageNumber.toString()),
      parseInt(req.query.pageItemCount.toString()),
    );

    if (!ads || ads.items.length < 1) throw new NoDataError();

    return new SuccessResponse('success', ads).send(res);
  }),
);

export default router;
