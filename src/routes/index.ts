import express from 'express';
import adsList from './ads/adsList';

const router = express.Router();

router.get('/', (req, res, next) => {
  res.status(200).send('Server main page....');
});

router.use('/ads/adsList', adsList);

export default router;
