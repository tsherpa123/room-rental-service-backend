'use strict';

import { Schema, model, Document, SchemaType } from 'mongoose';
import ILocation from './Location';
import IPoster from './Poster';
import IComment from './Comments';

export const DOCUMENT_NAME = 'Ads';
export const COLLECTION_NAME = 'ads';

export default interface IAds extends Document {
  views: number;
  pictures: string[];
  location: ILocation;
  price: number;
  title: string;
  description: string;
  poster: IPoster;
  comments: [IComment];
}

const schema = new Schema({
  views: {
    type: Schema.Types.Number,
    default: 0,
  },
  pictures: [{ type: String }],
  location: {
    type: Schema.Types.ObjectId,
    ref: 'ILocation',
  },
  price: {
    type: Schema.Types.Number,
    required: true,
  },
  title: {
    type: Schema.Types.String,
    maxlength: 100,
    minlength: 10,
  },
  description: {
    type: Schema.Types.String,
    maxlength: 500,
  },
  poster: {
    type: Schema.Types.ObjectId,
    ref: 'IPoster',
  },
  date: {
    type: Schema.Types.Date,
    default: Date.now(),
  },
  comments: [String],
});

export const AdsModel = model<IAds>(DOCUMENT_NAME, schema, COLLECTION_NAME);
