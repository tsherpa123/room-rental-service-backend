'use strict';

import { Schema, model, Document } from 'mongoose';

export const DOCUMENT_NAME = 'Location';
export const COLLECTION_NAME = '';

export default interface ILocation extends Document {
  id: string;
  Latitude: number;
  longitude: number;
  adress: string;
}

const schema = new Schema({
  id: {
      type: Schema.Types.ObjectId,
      auto: true
  },
  latitude: {
    type: Schema.Types.Decimal128,
  },
  longitude: {
    type: Schema.Types.Decimal128,
  },
  address: {
    type: String,
    minlength: 5,
    maxlength: 20,
  },
});

export const LocationModel = model<ILocation>(DOCUMENT_NAME, schema);
