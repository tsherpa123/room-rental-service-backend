'use strict';

import { Schema, model, Document } from 'mongoose';

export const DOCUMENT_NAME = 'Poster';
export const COLLECTION_NAME = '';

export default interface IPoster extends Document {
  id: string;
  firstName: string;
  lastName: string;
  emailAddress: string;
}

const schema = new Schema({
  id: {
    type: Schema.Types.ObjectId,
    auto: true,
  },
  firstName: {
    type: Schema.Types.String,
    maxlength: 20,
    minlength: 3,
  },
  lastName: {
    type: Schema.Types.String,
    maxlength: 20,
    minlength: 3,
  },
  // change it to email type for default validation
  emailAddress: {
    type: Schema.Types.String,
    maxlength: 200,
  },
});

export const AdsModel = model<IPoster>(DOCUMENT_NAME, schema);
