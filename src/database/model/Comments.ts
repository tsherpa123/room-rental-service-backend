'use strinct';
import { Schema, model, Document } from 'mongoose';

export const DOCUMENT_NAME = 'Comment';
export const COLLECTION_NAME = '';

export default interface IComment extends Document {
  id: string;
  date: Date;
  comment: string;
}

const schema = new Schema({
  id: {
    type: Schema.Types.ObjectId,
    auto: true,
  },
  date: {
    type: Schema.Types.Date,
    default: Date.now,
  },
  comment: {
    type: Schema.Types.String,
  },
});

export const CommentModel = model<IComment>(DOCUMENT_NAME, schema, COLLECTION_NAME);
