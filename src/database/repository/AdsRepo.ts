'use strict';

import IAds, { AdsModel } from '../model/Ads';
import { Types } from 'mongoose';
import ILocation from '../model/Location';
import IPoster from '../model/Poster';
import IPagination from '../../helpers/pagination';

export default class AdsRepo {
  /**
   * Add a new Ads
   * @param ads
   */
  public static async create(ads: IAds): Promise<IAds> {
    const createdAds = await AdsModel.create(ads);
    return createdAds.toObject();
  }

  public static async findByPagination(
    pageNumber: number,
    limit: number,
  ): Promise<IPagination<number, IAds[]>> {
    let list: IPagination<number, IAds[]> = {
      totalPages: 0,
      limit: limit,
      pageNumber: pageNumber,
      totalItems: await this.getTotalRegisters(),
      items: await this.getByPagination(pageNumber, limit),
    };

    //Calcule the number of pages.
    list.totalPages = Math.ceil(list.totalItems / limit);

    return list;
  }

  private static async getByPagination(pageNumber: number, limit: number): Promise<IAds[]> {
    return AdsModel.find()
      .skip(limit * (pageNumber - 1))
      .limit(limit)
      .sort({
        firsName: 'asc',
      })
      .lean<IAds>()
      .exec();
  }

  private static async getTotalRegisters(): Promise<number> {
    return AdsModel.find().countDocuments().exec();
  }
}
