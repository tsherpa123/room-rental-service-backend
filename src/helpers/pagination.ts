/**
 * Pagination of any kind of object
 */
export default interface IPagination<T, I> {
  totalItems: T;
  pageNumber: number;
  limit: number;
  items: I;
  totalPages: number;
}
