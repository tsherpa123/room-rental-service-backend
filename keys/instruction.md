# Add in this directory your RSA Keys

1. private.pem
2. public.pem

## How to generate the one certificate for testing 

### Generating the private key  
````
openssl genrsa -out room_rental_private.key 2048

````
### Extracting the Public Key 

````
openssl rsa -in room_rental_private.key -pubout -out room_rental_public.key
````

### Creating the CSR

After generating the private key, we are ready to create the CSR (Certificate Signature Request). The CSR is created using the PEM format and contains the public key portion of the private key as well as information about you (or your company).

````
openssl req -new -key room_rental_private.key -out room_rental.csr
````

